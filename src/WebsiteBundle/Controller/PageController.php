<?php

namespace WebsiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class PageController extends Controller
{

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request){

        $device = $this->get('mobile_detect.mobile_detector');
        $translator = $this->get('translator');

        $pagesRepository = $this->getDoctrine()->getRepository('AppBundle:Page');
        $serviceRepository = $this->getDoctrine()->getRepository('AppBundle:Service');
        $branchRepository = $this->getDoctrine()->getRepository('AppBundle:Branch');

        $branchQuery = $branchRepository->createQueryBuilder('b')
            ->where('b.isActive = true')
            ->join('AppBundle:BranchService', 'bs', 'WITH', 'bs.branch = b')
            ->join('AppBundle:Service', 's', 'WITH', 'bs.service = s')
            ->getQuery()
        ;
        $branches = $branchQuery->getResult();

        $homePage = $pagesRepository->findOneBy(array('pageSlug' => 'home'));
        $aboutPage = $pagesRepository->findOneBy(array('pageSlug' => 'about-us'));

        $services = $serviceRepository->findBy(
            array(),
            array(
                'serviceOrder' => 'ASC'
            )
        );

        $appMessage = "";

        if($device->isIphone()){
            $appMessage = $translator->trans('You can get our app from <a href="https://itunes.apple.com/am/app/black-bonus-card/id1273248724?mt=8">AppStore</a>');
        }

        if($device->isAndroidOS()){
            $appMessage = $translator->trans('You can get our app from <a href="https://play.google.com/store/apps/details?id=am.black.adam.black">Play Market</a>');
        }

        return $this->render('@WebsiteBundle/page/index.html.twig', array(
            'homepage' => $homePage,
            'aboutpage' => $aboutPage,
            'services' => $services,
            'branches' => $branches,
            'appMessage' => $appMessage
        ));

    }



}