<?php

namespace AppBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface;

class ConstModel
{

    // Transaction Type

    const TT_Create_Branch = 1;
    const TT_Create_Accumulative = 2;
    const TT_InterestReturn = 5;
    const TT_BonusReturn = 6;
    const TT_PaymentFromBonus = 7;
    const TT_PaymentBlackWithdraw = 8;
    const TT_BranchSale = 9;
    const TT_BranchExpense = 10;
    const TT_CreateSalesman = 11;

    // Account types

    const AT_Branch = 1;
    const AT_Accumulative = 2;
    const AT_Black = 3;
    const AT_BranchSale = 4;
    const AT_Salesman = 5;
    const AT_Card = 6;
    const AT_BlackBank = 7;

    // AccountStatus

    const AccStat_Waiting = 0;
    const AccStat_Active = 1;
    const AccStat_NotActive = 2;
    const AccStat_Closed = 3;

    // User Roles

    const User_CompanyManager = 1;
    const User_BranchManager = 2;
    const User_EntryPersonnel = 3;


    //error masige

    const Err_Bal_NoFounds = 101;
}