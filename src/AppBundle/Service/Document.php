<?php

namespace AppBundle\Service;

use AppBundle\Model\ConstModel;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Document
{

    private $em;
    private $trans;
    public $document;

    public function __construct(Transaction $transaction)
    {

        $this->trans = $transaction;
        $this->document = new \AppBundle\Entity\Document();
        $this->document->setDocumentDate(new \DateTime());
        $this->document->setAmount(0);

    }

    public function createDocument(EntityManager $entityManager, $transactionTypeId, $transactions = array()){

        $this->em = $entityManager;

        $transactionTypeRepository = $this->em->getRepository('AppBundle:TransactionType');
        $transType = $transactionTypeRepository->findOneById($transactionTypeId);

        if(!$transType){
            return new NotFoundHttpException('Transaction not found!');
        }

        $this->document->setTransactionType($transType);

        foreach($transactions as $trans){

            $this->trans->document = $this->document;
            $this->trans->account = $trans['account'];
            $this->trans->operation = (isset($trans['operation'])) ? $trans['operation'] : null;
            $this->trans->balance_change = (isset($trans['balance_change'])) ? $trans['balance_change'] : null;
            $this->trans->amount = (isset($trans['amount'])) ? $trans['amount'] : 0;

            $docTrans = $this->trans->createTransaction($this->em);
            $this->document->addTransaction($docTrans);

        }

        $this->em->persist($this->document);
        $this->em->flush();

        return $this->document;

    }

}