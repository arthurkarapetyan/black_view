<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
//use Sonata\TranslationBundle\Filter\TranslationFieldFilter;

class ServiceCategoryAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('serviceCategoryOrder')
            ->add('serviceCategoryTitle')
        ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('serviceCategoryOrder')
            ->add('serviceCategoryTitle')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('serviceCategoryOrder')
            ->add('serviceCategoryTitle')
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array()
                )
            ))
        ;
    }

}