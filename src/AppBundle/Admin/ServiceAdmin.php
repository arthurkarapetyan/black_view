<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;


class ServiceAdmin extends AbstractAdmin
{

    public $tbOptions = array(
        'multiple' => false,
        'image_directory' => '/img/service-icons',
        'thumbWidth' => 150,
        'thumbHeight' => 150,
        'cropOptions' => array(
            0 => array(
                'og' => array(
                    "title" => "Open Graph (facebook)",
                    "type" => "pixel",
                    "width" => 1200,
                    "height" => 630
                ),
                'thumb' => array(
                    "title" => "Thumbnail",
                    "type" => "pixel",
                    "width" => 150,
                    "height" => 150
                )
            )
        )
    );

    public function configure()
    {
        parent::configure();

        $this->setTemplate('edit', 'SonataAdminBundle:CRUD:tb_file_browser_edit.html.twig');

    }

    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('serviceCategory', 'sonata_type_model', array(
                'property' => 'serviceCategoryTitle'
            ))
            ->add('serviceOrder')
            ->add('serviceTitle')
            ->add('serviceIcon', FileBrowserType::class, array(
                'options' => array(
                    'multiple' => false
                )
            ))
        ;

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {

        $datagridMapper
            ->add('serviceCategory.serviceCategoryTitle')
            ->add('serviceOrder')
            ->add('serviceTitle')
            ->add('serviceIcon')
        ;

    }

    protected function configureListFields(ListMapper $listMapper)
    {

        $listMapper
            ->add('serviceCategory.serviceCategoryTitle')
            ->add('serviceOrder')
            ->add('serviceTitle')
            ->add('serviceIcon')
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array()
                )
            ))
        ;

    }

}