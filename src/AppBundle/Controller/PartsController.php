



<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class PartsController extends Controller
{

    public function footerContactAction(Request $request){

        $translator = $this->get('translator');

        $contactForm = $this->createFormBuilder()
            ->add('name', TextType::class)
            ->add('email', EmailType::class)
            ->add('message', TextareaType::class, array(
                'trim' => true
            ))
            ->add('save', SubmitType::class, array('label' => $translator->trans('Ուղարկել')))
            ->getForm()
        ;

        $contactForm->handleRequest($request);

        if($contactForm->isSubmitted() && $contactForm->isValid()) {

            $contactFormData = $contactForm->getData();

            $mail = \Swift_Message::newInstance()
                ->setSubject('Message from black.am')
                ->setFrom('info@black.am')
                ->setTo('info@black.am')
                ->setBody(
                    $this->renderView(
                        'emails/contact_form_footer.html.twig',
                        array(
                            'name' => $contactFormData['name'],
                            'email' => $contactFormData['email'],
                            'message' => $contactFormData['message']
                        )
                    ),
                    'text/html'
                )
            ;

            $this->get('mailer')->send($mail);

            $this->addFlash(
                'success',
                $translator->trans('Ձեր հաղորդագրությունը ուղարկված է')
            );

        }

        return $this->render('parts/footer_contact.html.twig', array(
            'contactForm' => $contactForm->createView()
        ));

    }

    public function requestCallbackAction(Request $request){

        $translator = $this->get('translator');

        $callbackForm = $this->createFormBuilder()
            ->add('phone', TextType::class)
            ->add('save', SubmitType::class, array('label' => $translator->trans('Պատվիրի՛ր զանգ')))
            ->getForm()
        ;

        $callbackForm->handleRequest($request);

        if($callbackForm->isSubmitted() && $callbackForm->isValid()) {

            $callbackFormData = $callbackForm->getData();

            $mail = \Swift_Message::newInstance()
                ->setSubject('Հետադարձ Զանգի Հարցում')
                ->setFrom('info@black.am')
                ->setTo('info@black.am')
                ->setBody(
                    $this->renderView(
                        'emails/request_callback.html.twig',
                        array(
                            'phone' => $callbackFormData['phone']
                        )
                    ),
                    'text/html'
                )
            ;

            $this->get('mailer')->send($mail);

            $this->addFlash(
                'success',
                $translator->trans('Ձեր հարցումն ուղարկված է, խնդրում ենք սպասել մեր մասնագետների զանգին')
            );

        }

        return $this->render('parts/request_callback.html.twig', array(
            'callbackForm' => $callbackForm->createView()
        ));

    }

}
