<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="service_category")
 */
class ServiceCategory
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Service", mappedBy="serviceCategory", cascade={"persist", "remove"})
     */
    private $service;

    /**
     * @ORM\Column(type="integer")
     */
    private $serviceCategoryOrder;

    /**
     * @ORM\Column(type="string")
     * @Gedmo\Slug(fields={"serviceCategoryTitle"}, updatable=true)
     */
    private $serviceCategorySlug;

    /**
     * @ORM\Column(type="string")
     */
    private $serviceCategoryTitle;

    public function __construct()
    {

        $this->service = new ArrayCollection();

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param mixed $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function getServiceCategoryOrder()
    {
        return $this->serviceCategoryOrder;
    }

    /**
     * @param mixed $serviceCategoryOrder
     */
    public function setServiceCategoryOrder($serviceCategoryOrder)
    {
        $this->serviceCategoryOrder = $serviceCategoryOrder;
    }

    /**
     * @return mixed
     */
    public function getServiceCategorySlug()
    {
        return $this->serviceCategorySlug;
    }

    /**
     * @param mixed $serviceCategorySlug
     */
    public function setServiceCategorySlug($serviceCategorySlug)
    {
        $this->serviceCategorySlug = $serviceCategorySlug;
    }

    /**
     * @return mixed
     */
    public function getServiceCategoryTitle()
    {
        return $this->serviceCategoryTitle;
    }

    /**
     * @param mixed $serviceCategoryTitle
     */
    public function setServiceCategoryTitle($serviceCategoryTitle)
    {
        $this->serviceCategoryTitle = $serviceCategoryTitle;
    }

}