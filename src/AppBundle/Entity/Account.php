<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="account")
 */
class Account
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User", inversedBy="account", cascade={"persist", "remove"})
     */
    private $user;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Customer", inversedBy="account", cascade={"persist", "remove"})
     */
    private $customer;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Branch", inversedBy="account")
     */
    private $branch;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Balance", mappedBy="account", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"balanceDate" = "DESC"})
     */
    private $balance;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AccountType")
     */
    private $accountType;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Document", mappedBy="accountFrom", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"documentDate" = "DESC"})
     */
    private $documentFrom;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Document", mappedBy="accountTo", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"documentDate" = "DESC"})
     */
    private $documentTo;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Transaction", mappedBy="account", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $transaction;

    /**
     * @ORM\Column(type="smallint")
     */
    private $accountStatus;

    /**
     * @ORM\Column(type="datetime")
     */
    private $accountCreatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $accountActivatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $accountClosedAt;

    public function __construct()
    {
        $this->documentFrom = new ArrayCollection();
        $this->documentTo = new ArrayCollection();
        $this->transaction = new ArrayCollection();
        $this->balance = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist(){

        $this->accountCreatedAt = new \DateTime();

    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate(){

        if($this->accountStatus == 2 && $this->accountActivatedAt == null){
            $this->accountActivatedAt = new \DateTime();
        }

        if($this->accountStatus == 3 && $this->accountClosedAt == null){
            $this->accountClosedAt = new \DateTime();
        }

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param mixed $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return mixed
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /**
     * @param mixed $branch
     */
    public function setBranch($branch)
    {
        $this->branch = $branch;
    }

    /**
     * @return mixed
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param mixed $balance
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
    }

    /**
     * @return mixed
     */
    public function getAccountType()
    {
        return $this->accountType;
    }

    /**
     * @param mixed $accountType
     */
    public function setAccountType($accountType)
    {
        $this->accountType = $accountType;
    }

    /**
     * @return mixed
     */
    public function getDocumentFrom()
    {
        return $this->documentFrom;
    }

    /**
     * @param mixed $documentFrom
     */
    public function setDocumentFrom($documentFrom)
    {
        $this->documentFrom = $documentFrom;
    }

    /**
     * @return mixed
     */
    public function getDocumentTo()
    {
        return $this->documentTo;
    }

    /**
     * @param mixed $documentTo
     */
    public function setDocumentTo($documentTo)
    {
        $this->documentTo = $documentTo;
    }

    /**
     * @return mixed
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param mixed $transaction
     */
    public function setTransaction($transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @return mixed
     */
    public function getAccountStatus()
    {
        return $this->accountStatus;
    }

    /**
     * @param mixed $accountStatus
     */
    public function setAccountStatus($accountStatus)
    {
        $this->accountStatus = $accountStatus;
    }

    /**
     * @return mixed
     */
    public function getAccountCreatedAt()
    {
        return $this->accountCreatedAt;
    }

    /**
     * @param mixed $accountCreatedAt
     */
    public function setAccountCreatedAt($accountCreatedAt)
    {
        $this->accountCreatedAt = $accountCreatedAt;
    }

    /**
     * @return mixed
     */
    public function getAccountActivatedAt()
    {
        return $this->accountActivatedAt;
    }

    /**
     * @param mixed $accountActivatedAt
     */
    public function setAccountActivatedAt($accountActivatedAt)
    {
        $this->accountActivatedAt = $accountActivatedAt;
    }

    /**
     * @return mixed
     */
    public function getAccountClosedAt()
    {
        return $this->accountClosedAt;
    }

    /**
     * @param mixed $accountClosedAt
     */
    public function setAccountClosedAt($accountClosedAt)
    {
        $this->accountClosedAt = $accountClosedAt;
    }

}