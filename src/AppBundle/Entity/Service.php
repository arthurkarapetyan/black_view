<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="service")
 */
class Service
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ServiceCategory", inversedBy="service")
     */
    private $serviceCategory;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\BranchService", mappedBy="service", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $branchService;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Document", mappedBy="service", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $document;

    /**
     * @ORM\Column(type="integer")
     */
    private $serviceOrder;

    /**
     * @ORM\Column(type="string")
     * @Gedmo\Slug(fields={"serviceTitle"}, updatable=true)
     */
    private $serviceSlug;

    /**
     * @ORM\Column(type="string")
     */
    private $serviceTitle;

    /**
     * @ORM\Column(type="string")
     */
    private $serviceIcon;

    public function __construct()
    {

        $this->branchService = new ArrayCollection();
        $this->document = new ArrayCollection();

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getServiceCategory()
    {
        return $this->serviceCategory;
    }

    /**
     * @param mixed $serviceCategory
     */
    public function setServiceCategory($serviceCategory)
    {
        $this->serviceCategory = $serviceCategory;
    }

    /**
     * @return mixed
     */
    public function getBranchService()
    {
        return $this->branchService;
    }

    /**
     * @param mixed $branchService
     */
    public function setBranchService($branchService)
    {
        $this->branchService = $branchService;
    }

    /**
     * @return mixed
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param mixed $document
     */
    public function setDocument($document)
    {
        $this->document = $document;
    }

    /**
     * @return mixed
     */
    public function getServiceOrder()
    {
        return $this->serviceOrder;
    }

    /**
     * @param mixed $serviceOrder
     */
    public function setServiceOrder($serviceOrder)
    {
        $this->serviceOrder = $serviceOrder;
    }

    /**
     * @return mixed
     */
    public function getServiceSlug()
    {
        return $this->serviceSlug;
    }

    /**
     * @param mixed $serviceSlug
     */
    public function setServiceSlug($serviceSlug)
    {
        $this->serviceSlug = $serviceSlug;
    }

    /**
     * @return mixed
     */
    public function getServiceTitle()
    {
        return $this->serviceTitle;
    }

    /**
     * @param mixed $serviceTitle
     */
    public function setServiceTitle($serviceTitle)
    {
        $this->serviceTitle = $serviceTitle;
    }

    /**
     * @return mixed
     */
    public function getServiceIcon()
    {
        return $this->serviceIcon;
    }

    /**
     * @param mixed $serviceIcon
     */
    public function setServiceIcon($serviceIcon)
    {
        $this->serviceIcon = $serviceIcon;
    }

}