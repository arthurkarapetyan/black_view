<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="account_type")
 */
class AccountType
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $accountTypeTitle;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return mixed
     */
    public function getAccountTypeTitle()
    {
        return $this->accountTypeTitle;
    }

    /**
     * @param mixed $accountTypeTitle
     */
    public function setAccountTypeTitle($accountTypeTitle)
    {
        $this->accountTypeTitle = $accountTypeTitle;
    }



}